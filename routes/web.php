<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


/**
 * Return all users in the system
 * In real project the route handler should be placed
 * in a controller to keep the routing file clean
 */
Route::get('/users', function() {
    return App\User::all();
});


Auth::routes();
