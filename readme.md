## Introduction

The salient purpose of the application is to demonstrate the concept of ACL using Laravel at backend and Vuejs on frontend.

An admin user has a provision to provide access of application pages and buttons to standard users. A page that allow to make these 
configurations is only visible to admin user.

## Demo URL

http://vue.sifars.com

Two users are automatically created with laravel seeder.

**Admin**

email: admin@gmail.com
password: admin@gmail.com

**Standard User**

email: sukhwinder@gmail.com
password: sukhwinder@gmail.com

## Dependencies

**Backend**

- Passport

**Frontend**

- Vuejs - A lightweight framework to create UI components (it does more than that)
- axios - Promise based http client to communicate with server
- vue-acl - used to define user permission on application routes
- vue-data-tables - Show data table with search and pagination
- vue-material - follow Google material design specs
- vue-router - Helps with application routes and acl
- vuelidate - validate user inputs before sending them to server
- vuex - vue store that helps to organize and mutate data from one place


## Frontend build details

laravel-mix makes it very easy to bundle the application assets together with minified js and css. The demo has been deployed  on nginx server with **gzip** compression
enabled to reduce the network bandwith and make the application load faster.

**prod build**

`npm run prod`

**dev serve**

`npm run watch`

**TODO** : Application vendor and user code has been bundled together within single file. it can be divided in vendor.js and app.js files.
Further the files can be versioned so that user has not to use hard refresh to get new changes made in the source code.


## Code base 

Below are the salient points of project code base:

`constant.js` file contains all the application constants and its object is available in each vue component by using `this.$constant`
so that we don't need to import it in each file. Make sure to change **baseURL** according to your backend address and listening port.

`http-common.js` creates an axios instance and register a request interceprtor in it to append access token with each request.
import HTTP from this file to make reqeusts to server

`auth.js` carries helper methods that helps to store the access token and delete it, use it to see whether user is logged in or not

User permissions are stored in local storage and this is managed by `user-permissions.js`

`router.js` carries application url and a beforeEach guard to prevent unauthenticated users to access authenticated routes.

`PermissionDenied.vue` is used to show standard user that they don't have access to a particular page

`Settings.vue` hold the ACL configuration logic

## Application setup details

Since, we have used laravel passport to authenticate users, it needs to have client information with authentication request. Make sure to provide
client_id and client_secret in Login.vue file.

Rest of setup is similar to setup any laravel-passport app.

Create an issue in the repo if you found any issues setting up the application.




