<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        
        \DB::table('users')->insert([
            0 => 
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'password' => '$2y$10$VEFZ7UPZCyuuAJQI16RC5O7h7o2SLEaDFP7AomZKjcR/F2LpoInaO',
                'remember_token' => 'null',
                'created_at' => '2017-06-25 18:00:56'
            ],
            1 => 
            [
                'id' => 2,
                'name' => 'Sukhwinder',
                'email' => 'sukhwinder@gmail.com',
                'password' => '$2y$10$Bmmjj.jqNmJhXcVR8e4UceUPShcdBiXipnGBCZfDNo3Zv14KdYPcK',
                'remember_token' => 'null',
                'created_at' => '2017-06-25 18:00:56'
            ]
        ]);
        
    }
}
